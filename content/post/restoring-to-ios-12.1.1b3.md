---
title: "Restoring to iOS 12.1.1b3"
date: 2019-03-04T17:56:27-05:00
subtitle: "Detailed guide for beginners"
author: "Luke Chambers"
tags: ["12", "downgrade"]
draft: false
---

### Updates

* **3/7/2019:** Apple is no longer signing iOS 12.1.1 beta 3, meaning this guide will **not** work.

If you're on a non-jailbreakable iOS version and want to downgrade or are on iOS 11 or below and want to upgrade to iOS 12 and jailbreak, you can currently do that by restoring to iOS 12.1.1 beta 3 using iTunes. Apple is currently still signing this version of iOS for some reason and it also happens to be jailbreakable using unc0ver. You should do this as soon as possible though because Apple could close the signing window at any time.

### Requirements

* An iOS device that can run iOS 12
* A computer that can run the latest version of iTunes
* iTunes installed on your computer
* A lightning to USB-A cable and the appropriate dongles if necessary
* An internet connection

### Warnings

* Any time you restore your device, all data is wiped, including apps, settings, messages, etc.
* Don't unplug your device from your computer or close iTunes while your device is restoring.

### Steps

1. On your computer, go to the beta firmware [page](https://www.theiphonewiki.com/wiki/Beta_Firmware) on The iPhone Wiki and find your device type. Under there, you should see a list of major iOS versions. Choose "12.x". You should now be on a page with a table for each device. Find your device on that page and scroll down until you find the "12.1.1 beta 3" row. Look to the second to last column and you should see a link to a file download. Click on it and wait for that file to download. That's the IPSW file you're going to be restoring to.

2. Once the IPSW is done downloading, plug your device into your computer using your lightning to USB-A cable (and appropriate dongles if necessary) and open iTunes on your computer. Look near the top left of the iTunes window for a button with an icon that matches your device. For example, if you're using an iPhone, you'll look for a button with an iPhone icon on it. Once you find that button, click on it. If you're brought to a screen that says to finish setting up your device, go through that and you should get to the main device page. If you're brought right to the main device page, you don't need to do anything extra.

3. Once you're in the main device page in iTunes, look for a button in the first gray box that starts with "Restore" and has your device type after that. For example, if you're using an iPhone, you'll look for a button that says "Restore iPhone...". Once you've found that button, hold down the shift key on your keyboard and click on that restore button. It should bring up a file selection window. In that window, select the IPSW file you downloaded in step one. You might be prompted to agree to Apple's terms and conditions or something similar. If you are, agree to them. Your device should now be restoring.

4. Once your device is done restoring, you can follow the on-device setup process and you should be good to go. Make sure to block all updates using the tvOS profile and delete any OTA updates if there are any to make sure you don't accidentally update to the latest version of iOS.